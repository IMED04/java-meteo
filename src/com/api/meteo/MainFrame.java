package com.api.meteo;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.swing.*;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainFrame extends JFrame {
     ;


    public  MainFrame(String title) throws IOException {
        super(title);
        String apiKey="0c4a67f646ac5bce20b7aa10d1947001";
        double latitude=37.8267;
        double longitude=-122.4233 ;
        String  forecasturl=String.format("https://api.darksky.net/forecast/%s/%.4f,%.4f",apiKey,latitude,longitude);
//       String forecasturl="https://api.darksky.net/forecast/"+apiKey+"/"+latitude+","+longitude;
        System.out.println("Avant la réquete");
        new ForecastWorker(forecasturl).execute();
        System.out.println("Aprés la réquete");

    }




        class ForecastWorker extends  SwingWorker<String ,Void>{
        private String forecasturl;
        public  ForecastWorker(String forecasturl){
            this.forecasturl =forecasturl;
        }


            @Override
            protected String doInBackground() throws Exception {

                OkHttpClient client=new OkHttpClient();
                Request request=new Request.Builder()
                        .url(forecasturl)
                        .build();
                Call call=client.newCall(request);
                try {
                    Response response=call.execute();
                    if(response.isSuccessful()){

                        return response.body().string();
                    }

                }catch (IOException e){
                    System.err.println("Error" + e);
                }

                return null;
            }
            @Override
            protected void process(List<Void> chunks) {
                super.process(chunks);
            }

            @Override
            protected void done() {
                super.done();
                try {
                    System.out.println(get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }


}
