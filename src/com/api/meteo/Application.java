package com.api.meteo;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        System.out.println(Thread.currentThread().getName());
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                MainFrame mainFrame= null;
                try {
                    mainFrame = new MainFrame("meteo");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mainFrame.setResizable(false);
                mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                mainFrame.pack();
                mainFrame.setLocationRelativeTo(null);
                mainFrame.setVisible(true);

            }
        });

    }
}
